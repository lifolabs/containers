#  Containers

## LXC

## Docker


How to use Docker in a practical way (part 1 - Introduction)
On this page

    Part 1: Introduction to the terminology
        Preface
        How did containers came up
        Virtualization
        Containers
        Docker
        Summary 

Docker
Part 1: Introduction to the terminology
Preface

It is not an uncommon situation, for early adopters of newly introduced concepts and technologies, to be totally confused when these can fundamentally change the ways of developing and delivering services. Especially when everybody talks about something like Docker, and how awesome and game changing it is. This confusion happens when we try things early on and rushing straight to testing them without grasping the whole concept and background of this newly introduced technology.

This is why you may have abandoned the whole trend of Linux containers, or because you read some controversial article from yea sayers and naysayers. In this first part, of a series of articles, we will try our best to clear things up and put everything on the right perspective for any developer, sysadmins, Q/A engineers or even enthusiasts who just need the right inspiration to use Linux containers and solve their special IT problems.

We will start from the beginning, with some necessary description of the historical events and concepts and then I will showcase how we can start working with Docker containers. This way, you will be able to understand "what led to the creation of containers", "what are the building blocks" and "how do they work".

How did containers came up

For many years, servers were used as they are, with a single hardware configuration, containing a single operating system (OS for short), to provide thousands of websites and some early versions of enterprise software on the same machine.

To accomplish the necessary scaling, companies needed to buy new hardware and plan carefully the steps for any new implementations. In this period of time, automation was in its early days and most of the configurations were done by hand.

Virtualization

Then virtualization came along which let people utilize the same hardware server so they could spawn multiple virtual servers for different purposes and sell them individually. This has not only, enormously reduced the costs of server hardware ownership, but also provided the means to automate and easily mange hundreds of server instances.

Virtualization, is actually assembling hardware components like CPU, RAM, Disks, Network Cards etc that are emulated by a special software. It is like building a PC, with its core components mentioned above, within an operating system and assemble them to work as if it was a real PC. This way, the virtual PC becomes a "guest" inside an actual PC which with its operating system is called a host.

Virtualization

Although hardware virtualization was introduced roughly 10 years ago, it got its widespread use on various types of servers by developers and sysadmins, using the KVM (Kernel-based Virtual Machine) feature that is built in the Linux Kernel since 2007. With Intel's "VT-x" and AMD's "AMD-V" extensions that help create any number of virtualized operating systems (limited just by the hardware resources), every developer could cheaply create multiple virtual machines (VM for short) with OS's, even with different versions of them. This way they are able to test out their applications without the need to buy a whole new rig or multiple of them, depending on the OS they need.

One of the big advantages of Virtualization is the ability to create snapshots. A snapshot is the state of a virtual machine at an exact point in time. You can think of it as a “frozen” state of the virtual machine at a particular time. This way a developer or a sysadmin can create a virtual machine, install a particular OS and the tools that he needs and the create a snapshot out of the whole thing. Then he can start testing, configuring or any tasks that he needs to accomplish and then revert back to the previous snapshot instantly, at any given time.

Also Virtual Machines can be migrated from one host machine to another and continue their working state without any special configuration. This is because, the entire virtual machine is actually some huge files which are usually called images. You can think of images, like your favorite Linux distributions ISO file. As the ISO file contains all the essential components to actually run an entire OS from a live USB/DVD, the same way a virtual machine image, contains the OS and virtual hardware components.

All the above, created an entire industry of Virtual Private Server (VPS) hosting companies where the customers could instantly launch pre-configured images of servers for any purpose. These VPS providers usually have 10 or 20 snapshots of VM's on a main host server that are cloned multiple times per customer request to provide them the virtual servers that they need
Containers

As you can imagine a Virtual machine is an entire operating system running inside a host operating system. The guest operating systems, although they are isolated, they share and utilize the hardware resources of the host machine. Some times, there are multiple guest operating systems that are running the same whole stack of a specific operating system.

Containers vs. Virtual Machines.

This situation, created an opportunity for the Linux kernel developers and hackers to come up with an idea that is called lightweight process virtualization. So, instead of using an entire operating system, they could cut down the "unnecessary" components of the virtual OS to create a minimal version of it. This lead to the creation of LXC (Linux Containers).

Before we dig deeper, we should mention that the lightweight process virtualization is not a new thing. Solaris has Zones, BSD has jails and there are other similar technologies like OpenVZ. The problem is that they frequently change their name or purpose when the same base concept is used on other projects. It's true that not all are the same, but the fundamental principles are pretty much the same. They all want to isolate, deploy and create a disposable way of delivering software services without the hassle of rebuilding everything and every time from the bottom up.

The LXC project differs from the aforementioned Virtual machines in that it is an operating system level virtualization environment and not a hardware virtualization environment. They both do the same thing, but LXC provides operating system-level virtualization through a virtual environment that has its own process and network space, instead of creating a full-fledged virtual machine. As a consequence, a LXC virtual OS has minimal resource requirements and it boots in a matter of seconds.

As you can see in the following image, the LXC virtual Ubuntu on the left, uses 11MB in a default installation.

LXC vs. Host
Docker

As usually, things didn't stop just by removing the unnecessary parts of an OS. Various technologies came to life by pushing the boundaries of lightweight process virtualization even further. In 2013, developers of dotCloud (a company which later changed its name to Docker Inc.) introduced Docker.

Docker is an open source engine which its primary focus is to automate the deployment of applications inside software containers and the automation of operating system level virtualization on Linux. A docker container, unlike a virtual machine and lxc, does not require or include a separate operating system. Instead, it relies on the Linux kernel's functionality and uses resource isolation.

Docker containers are created from docker images (remember the snapshots). You can imagine a docker container as the live state of a web application running from an iso file. But this time the iso, which in our example is the equivalent of the docker image, contains just the application and its dependencies.

Docker Scheme

One great feature of docker, which we will discuss on the second part, is the docker file. A docker file is the recipe that contains all the necessary steps that are needed to create a docker image. There are literally tons of “ready to use” docker files that you can customize are use them as they are.
Summary

As you can imagine, the scope of getting from full-fledged servers, to OS virtualization and then to containers is to remove the burden of building, deploying and maintaining an entire operating system when what they only need is just the application layer.

With this introduction, we tried to present you with some fundamental events that led us to the creation of the Docker containers. I also tried to simplify some of its concepts, so that you can understand the differences between the various virtualization technologies and where they are applicable.

The second part which will get published next week, will showcase exactly how we can install and use Docker containers in a practical way, so stay tuned.



---

Docker Tutorial Part 2: How to use it in a practical way
On this page

    Preface
    Installing Docker
    Prerequisites
    Installing Docker engine on Ubuntu 15.10
    Managing Docker service on Ubuntu 15.10
    Summary

Part 2: Docker installation and service management.
Preface

In the first part, I presented the fundamental ideas behind Docker containers and how exactly they work. In this second part, we will proceed with the installation of Docker and its management as a service in our system. We will prepare our system so that in the next part we can create a personal notepad using the WordPress content management system (CMS) or the Dokuwiki which is a wiki software that doesn't require a database.

As we discussed in the first part, to accomplish the above tasks, we would have to either manually install and configure a physical machine with the Apache, MySQL, PHP parts that are needed in order to run the Wordpress CMS or the Docuwiki, or install a Linux server distribution in a virtual machine and then install and configure Apache, MySQL, PHP.

With the docker containers, we don't have to do all the manual labor. We just need to download the prebuilt image and run it in a container that has all the stuff that we need, pre-configured for us and ready to be ran. But let's just focus on our system preparation first.

Docker installation and service management.

Installing Docker

Before we start, we need to prepare our physical machine with some prerequisites for the docker service. I will describe the procedure for the Ubuntu Linux operating system, but the same applies to any distribution really, with only slight changes in the package installation commands. Currently, Docker is supported on Ubuntu 15.10/14.04/12.04. For other distributions, you can check the official documentation (https://docs.docker.com/engine/installation/linux/).

Prerequisites

Docker requires a 64-bit installation regardless of your Ubuntu version. Additionally, your kernel must be on 3.10 version at minimum, because Linux kernels older than 3.10 lack some of the features required to run Docker containers. These older versions are known to have bugs which cause data loss and frequently panic under certain conditions.

Installing Docker engine on Ubuntu 15.10

We will install the Docker engine from the official repositories because they regularly release new versions with new features and bug fixes while the Docker on the Ubuntu repositories is usually several versions older and not maintained.

If you have previously installed Docker on your Ubuntu installation from the default Ubuntu repositories, you should purge it first using the following command:

sudo apt-get --purge autoremove lxc-docker

Docker’s apt repository as of this writing it contains the Docker engine 1.10.1 version. Now let us set apt to use packages from the official repository:

1) Open a terminal window.
2) Add the corresponding gpg key for the Docker repository

sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

3) Edit the /etc/apt/sources.list.d/docker.list file in your favorite editor. You can ignore if it doesn't exist, we can safely create it.

sudo nano /etc/apt/sources.list.d/docker.list

Add the following line in the docker.list file

deb https://apt.dockerproject.org/repo ubuntu-wily main

Save and close the /etc/apt/sources.list.d/docker.list file.

4) Now that the new repository is added you should update the apt package index.

sudo apt-get update

5) First you should install the `linux-image-extra kernel` package. The Linux-image-extra package allows docker to use the aufs storage driver

sudo apt-get install linux-image-extra-$(uname -r)

6) Now you can install the docker engine

sudo apt-get install docker-engine

You can verify that apt is pulling docker engine from the official repository with the following command:

apt-cache policy docker-engine

Install Docker

With the above command, you will see the version of the docker, which should probably be 1.10.1+ and some entries that indicate the official origin of the docker package. If the information is correct and you see links to the official Docker repositories then whenever you run sudo apt-get upgrade, your system will pull the new versions from the official repository.

Managing Docker service on Ubuntu 15.10

Now that we have our system prepared let's discuss the management of the Docker service that runs in the background.

First things first, we should learn how to start or stop the Docker service and also how to check if it is running with the systemctl tool.

To check if the docker is running and also check some useful information about our memory, CPU, process ID and some log entries, we can run:

sudo systemctl status docker

To start the Docker service, we issue the following command:

sudo systemctl start docker

Start Docker

To stop the Docker service, we issue the following command:

sudo systemctl stop docker

Stop Docker

If for any reason we do not want the Docker service to run always in the background, we can disable its startup during system boot by issuing the following command:

sudo systemctl disable docker

If we want to revert the above action we can enable the Docker service to start during system boot with the following command:

sudo systemctl enable docker

Summary

With the second part, we have concluded our preparation of the underlying operating system (Ubuntu 15.10 in our case) to be able to run the latest version of Docker engine. Also, we learned how to start, stop, check the status of the Docker service and either enable or disable its startup during the system boot.
In the next (third) part, we will start using Docker images and see how we can create containers in a practical way.

---



Docker: How to use it in a practical way - Part 3
On this page

    How to run a Docker container
    Introduction to the Docker Hub
    Searching for Docker Images
    Downloading a Docker Image
    The Docker Image architecture
    Deleting a Docker Image
    Containers are ephemeral
    Docker container Networking
    Creating a personal notepad with a WordPress container
    Creating persistent storage
    Creating a personal notepad with a DokuWiki container
    Deleting a Docker container
    Summary

Part 3: Creation a Notepad with WordPress and DokuWiki containers

Preface

In the first part, we talked about how docker containers work and differ from other software virtualization technologies and in the second part , we prepared our system for managing docker containers.

In this part, we will start using Docker images and create containers in a practical way. In other words, we will create a web-based, advanced personal notepad that runs on top of DokuWiki or WordPress. You can choose whichever you are comfortable with.

Docker Container Virtualisation

How to run a Docker container

First we must make sure that docker engine is working as needed by downloading a "Hello world" image and creating a container from it.

Remember, when we talk about an image it is the suspended state whereas when we talk about container it is a run-time instance of a docker image. In an analogy, that we talked in a previous part, a docker image is like the ISO file of a Linux distribution, while the container is the live session of the ISO file as if you were running it from a USB thumb drive.

To download and run the "Hello world" image just type in the terminal

sudo docker run hello-world

This command downloads the Hello World image and runs it in a container. When the container runs, it prints an informational message and then, it exits (meaning it shut down).

Docker Hello World example.

How do we check how many images do we have in our system? Well we simply run

sudo docker images

Show Docker images.

You may ask yourself, how did my system find this Hello World image and where did it come from? Well, this is where the docker hub comes in play.

Introduction to the Docker Hub

The Docker Hub is a cloud-based centralized resource for container image discovery, image building, and distribution of those images.
Specifically, Docker Hub provides some useful features and functions which we will discuss more in later parts.

Currently, we will focus on one feature and this is finding and downloading a docker image.

Searching for Docker Images

You can search for a "ready to download and run docker image", by simply visiting the online docker hub or by using the terminal. Note that you can not download a docker image from the web hub but you can learn more about an image, like how it is built and maintained etc.

So for the purpose of this part, we will focus on using the terminal way. Let us search for WordPress

sudo docker search wordpress

Search for Docker images.

As you can see there are tons of WordPress docker images, that are built with various combinations (e.g. with or without database inclusion), they are rated with popularity stars and they are either official (maintained by the docker company) or automated (built and maintained by individuals). It is obvious that anyone can create an account and upload his / her custom docker image and we will discuss it in a later part.

Downloading a Docker Image

For the purpose of this article, we will use the latest built of bitnamis' docker image, which comes with MySQL preinstalled in it. So let us download it:

sudo docker pull bitnami/wordpress:latest

Once you run the above command, it will communicate with the docker hub, ask if there is a repository named "bitnami", then asks if there is a "WordPress" build that is tagged as the "latest" version.

Download Docker image.

Currently, we have downloaded a WordPress image and nothing else. We can proceed now by downloading a DokuWiki Image by searching one and selecting the one that we like, or using the one that is as follows

sudo docker pull mprasil/dokuwiki

The Docker Image architecture

While waiting for the download procedure to complete, you can see that a docker image is a multi-layer image on top of a base image. You can see each and every layer being downloaded and then "magically" be unified. The diagram below shows an Ubuntu base image comprising 4 stacked image layers.

Docker Architecture - Part 1

As you can imagine, each Docker Image references a list of read-only layers that represent file-system differences. When you create a new container, from a Docker Image as we will do, later on, you add a new, thin, writable layer on top of the underlying stack. All changes made to the running container - such as writing new files, modifying existing files, and deleting files - are written to this thin writable container layer. The diagram below shows a container based on the Ubuntu 15.04 image.

Docker Architecture - Layers.

Deleting a Docker Image

Now if you check how many images you have on your system

sudo docker images

you will see the WordPress, DokuWiki, and the Hello World. If for any reason you want to remove and delete (rmi) an image you simply type

sudo docker rmi <name-of-the-image>

where the name of the image is the name of the docker as it is displayed with "docker images" command. For example, if we want to delete the Hello World we can simply type:

sudo docker rmi hello-world

Containers are ephemeral

By design Docker containers are ephemeral. By “ephemeral,” we mean that a container can be stopped and destroyed and a new one can be built from the same Docker image and put in place with an absolute minimum of set-up and configuration.

Thus, you should keep in mind that when we will create a container from the Docker Image of your preference (WordPress or DokuWiki) any changes you make e.g. adding a post, picture, will be lost once you stop or delete the container. In other words, when a container is deleted, any data written to the container that is not stored in a data volume is deleted along with the container.

A data volume is a directory or file in the Docker host’s filesystem that is mounted directly into a container. This way you can swap containers, with new ones and keep any data safe in your users home folder. Note that, you can mount any number of data volumes into a container. Even multiple containers can also share one or more data volumes.

The diagram below shows a single Docker host (e.g. your Ubuntu 15.10) running two containers. As you can see there is also a single shared data volume located at /data on the Docker host. This is mounted directly into both containers.

Docker Container Data

This way when a container is deleted, any data stored in data volumes persists on the Docker host and can be mounted to a new container.

Docker container Networking

When you install Docker, it creates a network device in your system. You can view it (it will be named as docker0) as part of a host’s network stack by using the `ifconfig` command on your host system.

It is important to understand that Docker containers are isolated and they are individual micro-services that have their own network properties and the way we run them and connect to them is by mapping their port number to a port number of the hosts system.

This way we can expose the web service that a container runs to the host system.

Creating a personal notepad with a WordPress container

Let us get started with creating our testing notepad. First we will use the WordPress image to create a Docker container

sudo docker run --name=mynotepad -p 80:80 -p 443:443 bitnami/wordpress

With the above command, we asked the Docker service in our host system to create and run (docker run) a container named `mynotepad` (--name=mynotepad), map the HTTP and HTTPS port of the host and container ( -p 80:80 -p 443:443 ) and use the WordPress image ( bitnami/wordpress )

Docker Wordpress Container

Once the container is initialized you will be greeted with some info about the container. It is time to launch a browser and point it to http://localhost

If everything went well, you will see the default WordPress website

Wordpress running in Docker.

As you may already know to log in to the WordPress administration page, just go to http://localhost/login and use the default credentials user / bitnami. Then you can create a new user or a test post in the WordPress and publish it. You can see my test post in the image below

Wordpress in Docker

Let us get back to the terminal. As you can see your terminal currently is bind to the running container. You can use Ctrl+C to exit. This will also stop the container.

Now let us check our available containers. You can run the following command:

sudo docker ps -l

to view the container that we had previously created and run.

As you can see from the above image, there is some important information like the name of the container and the unique ID of the container. This way we can start the container again:

docker start mynotepad

Then you can check the processes that the docker container runs, with the following command:

sudo docker top mynotepad

By default with the `docker start mynotepad` the docker container is running in the background. To stop it, you can run the following command

sudo docker stop mynotepad

You can read more on how to interact with the container in the official documentation of the docker https://docs.docker.com/engine/userguide/containers/usingdocker/

Where are the containers

If you want to see where the containers are on the hosts file system then you can head over to /var/lib/docker

sudo cd /var/lib/docker
sudo ls
sudo cd containers
sudo cd ID
sudo ls

As you can see the ID numbers represent the actual containers that you have created.

Creating persistent storage

Let us create a new WordPress container, but this time, will put it in the background and also expose the WordPress folder to our host system so that we can put files in it or remove any files we don't want.

First we create a folder in our home directory

mkdir ~/wordpress-files

then run and create a container based on the same image we created the previous one:

sudo docker run -d -ti --name=mynotepad-v2 -v ~/wordpress-files:/opt/bitnami/apps -e USER_UID=`id -u` -p 80:80 bitnami/wordpress

Docker Persistant Storage.

The difference, this time, is that we used the -d parameter for detached mode and the -ti parameter to attach a terminal in interactive mode so that I can interact with it later on.

To check the running container just run the following command

sudo docker ps

Let's stop the container

sudo docker stop mynotepad-v2

Now if you run the `docker ps` command you will see nothing there.
Let's start it again with the following command:

sudo docker start mynotepad-v2

If you check the folder we have previously created you will see the WordPress installation

You can read more about the image we used at the docker hub https://hub.docker.com/r/bitnami/wordpress/

Creating a personal notepad with a DokuWiki container

This time, we will create a notepad using DokuWiki. As we have previously downloaded the image, the only thing that's left to be done is to create a container out of it.

So let's run the following command to create our `mywikipad` named container

docker run -d -p 80:80 --name mywikipad mprasil/dokuwiki

And then head over to your browser and add the following address to start the configuration of your wiki notepad:

http://localhost/install.php

You can learn more for DokuWiki from the official documentation and customize the wiki for your needs:

https://www.dokuwiki.org/manual

Dokuwiki in Docker

Deleting a Docker container

Once you are comfortable with creating, starting and stopping docker containers, you will find yourself in need to clean up the testing mess created by the multiple containers.

To delete a container first you will need to stop it and then delete it by running the following command:

docker rm <name of container or ID>

You can also add multiple ID's in the same `docker rm` command to delete multiple docker containers at the same time.

Summary

In this part, we learned how to create a container and use it in a practical way to create a personal notepad based on either WordPress or DokuWiki. We looked at some basic commands on how to start and stop the containers that we create and how to delete the images and the containers.

In the next part, we will take a look on how the docker images are created by creating our own.


---


Home
Docker Part 4: building and publishing custom docker images
>
Scan your Web-Server for Malware with ISPProtect now. Get Free Trial.
Docker Part 4: building and publishing custom docker images
On this page

    Introduction to Docker Files
    Building a Docker Image
    Building Locally using a Dockerfile
    Publishing your Custom Docker Image on Docker Hub
    Downloading your Custom Image
    Summary

In the first and second part, we covered the fundamentals of running a container by preparing our system for managing docker containers and what exactly it means to use a docker container against other virtualization technologies from a technical perspective. Then, on the third part, we went hands on with docker images, learned the basics and created our first Notepad container using either WordPress or DokuWiki.

In this fourth part, we are going to see how docker images are built and we will create our own custom image ready to be downloaded and shared with our friends, colleagues and communities.

For this tutorial, we will use the Whale Docker image. The Whale is the official mascot of Docker and the Whale docker image resembles the cowsay program which generates ASCII pictures of a cow in the terminal with a message. It can also generate pictures using pre-made images of other animals, such as Tux the Penguin, the Linux mascot.

Docker

Introduction to Docker Files

In the previous part, we introduced ourselves to the Docker Hub and learned that it is a cloud-based centralized resource for container image discovery, image building, and distribution of those images. We also learned that a docker image is a multi-layer image on top of a base image. That said, using a base image removes the hassle of creating from scratch a suitable OS for docker image and gives us the option to customize the upper layers where our software will reside.

To create a custom image by using a base image, we need to provide the docker engine with instructions on how to install and configure packages and files and also some settings that go with it. These instructions are written in a plain text file called “dockerfile”. Dockerfiles are like recipes that you hand over to a chef and he/she cooks you a great meal. These docker files are written using a simple, descriptive set of steps that are called “instructions”. Each instruction creates a new layer in our image. Instructions include actions like running a command, adding a file or directory, what process to run when launching a container from the image, etc. This process is just like you would set a series of commands in a shell script. Once you request the building of an image, the Docker reads the dockerfile executes the instructions, and returns a final image.

Building a Docker Image

There are two ways of building your custom Docker image. You can either build it on your computer or you can use the Docker Hub that we talked about previously. In this part, we will learn how to build our Docker image locally and then publish it on the Docker Hub Registry.

Building Locally using a Dockerfile

We will create a custom Docker Image using the "Whale say " image which is a small Docker Image (based on an Ubuntu Image) which when you run it, it says something that you programmed to say back to you.

First, fire up a terminal and create a new folder by typing:

mkdir mywhale

This directory serves as the “context” for your build. The context just means that it contains all the things that you need in order to successfully build your image.

Get inside to your new folder with:

cd mywhale

and create a Dockerfile in the folder by typing:

touch Dockerfile

Now you should see the empty Dockerfile that we created if you give 'll' command:

ll

$ ll
total 8.0K
-rw-rw-r-- 1 user user 0 23:26 Dockerfile

Open it with your favorite text editor and add:

FROM docker/whalesay:latest

This first line of instruction, with the FROM keyword, tells Docker which image your image is based on. You are basing your new work on the existing whalesay image.

The next instruction that we will add will give the ability to our whale to tell a fortune. To accomplish this task, we will use the fortune package that is available in the Ubuntu repositories (remember that the whale image is based on a Ubuntu image). The fortunes program has a command that prints out wise sayings for our whale to say.

So, the first step is to install it. To do this we add the usual apt install instruction:

RUN apt -y update && apt -y install fortunes

Once the image has the software it needs, you instruct the software to run when the image is loaded. To do this we add the following instruction:

CMD /usr/games/fortune -a | cowsay

The above line tells the fortune program to send a randomly chosen quote to the cowsay program

And we are done! Now save the file and exit.
You can verify what you did by running "cat Dockerfile" so as that your Dockerfile looks like this:

cat Dockerfile

FROM docker/whalesay:latest
RUN apt-get -y update && apt-get install -y fortunes
CMD /usr/games/fortune -a | cowsay

Now that everything (hopefully) looks good, its time to build our Docker Image (don’t forget the . period at the and of the command).:

docker build -t my-docker-whale .

The above command takes the Dockerfile in the current folder and builds an image called “my-docker-whale” on your local machine.

You can verify that your Docker image is indeed stored on your computer with:

docker images

Then you may run your Docker image by typing the following:

docker run my-docker-whale

Once it runs, you will get something like the following image

The Docker Whale image

Publishing your Custom Docker Image on Docker Hub

Your next option is to publish the created Docker image on the Docker Hub Repository. To do so, you will need to create an account on the Docker Hub signup webpage where you will provide a name, password, and email address for your account. I should also point out that the Docker Hub service is free for public docker images. Once you have created your account, you can push the image that you have previously created, to make it available for others to use.

To do so, you will need the ID and the TAG of your “my-docker-whale” image.

Run again the "docker images" command and note the ID and the TAG of your Docker image e.g. a69f3f5e1a31.

Now, with the following command, we will prepare our Docker Image for its journey to the outside world (the accountname part of the command is your account name on the Docker Hube profile page):

docker tag a69f3f5e1a31 accountname/my-docker-whale:latest

Run the "docker images" command and verify your newly tagged image.

Next, use the "docker login" command to log into the Docker Hub from the command line.

The format for the login command is:

docker login --username=yourhubusername --email=youremail@provider.com

When prompted, enter your password and press enter.

Now you can push your image to the newly created repository:

docker push accountname/my-docker-whale

The above command can take a while to complete depending on your connection's upload bandwidth as it uploads something like 180ΜΒ of data (in our example). Once it has completed, you can go to your profile on Docker Hub and check out your new image.

Downloading your Custom Image

If you want to pull your image from your Docker Hub repository you will need to first delete the original image from your local machine because Docker would refuse to pull from the hub as the local and the remote images are identical.

As you remember from the previous part, to remove a docker image, you must run the "docker rmi" command. You can use an ID or the name to remove an image:

docker rmi -f a69f3f5e1a31

Now that the image is deleted you can pull and load the image from your repository using the "docker run" command by including your account name from Docker Hub.

docker run accountname/my-docker-whale

Since we previously deleted the image and it was no longer available on our local system, Docker will download it and store it in the designated location.

Summary

In this part, we learned how to create a Docker file, write some basic instructions and the build and image using it as a recipe. We also covered the basics of pushing our new custom image to the Docker Hub registry and the pulling it to our local machine. In the next part of this series of Docker tutorials, I will showcase how we can automate the image building procedure so that we can instantly create custom images any time we need them! Stay tuned.

---

Dockerizing LEMP Stack with Docker-Compose on Ubuntu
On this page

    Step 1 - Install Docker
    Step 2 - Install Docker-Compose
    Step 3 - Create and Configure the Docker Environment
    Step 4 - Configuration of the docker-compose.yml file
    Step 5 - Run Docker-Compose
    Step 6 - Testing
        Reference 

Docker-Compose is a command line tool for defining and managing multi-container docker applications. Compose is a python script, it can be installed with the pip command easily (pip is the command to install Python software from the python package repository). With compose, we can run multiple docker containers with a single command. It allows you to create a container as a service, great for your development, testing and staging environment.

In this tutorial, I will guide you step-by-step to use docker-compose to create a LEMP Stack environment (LEMP = Linux - Nginx - MySQL - PHP). We will run all components in different Docker containers, we set up a Nginx container, PHP container, PHPMyAdmin container, and a MySQL/MariaDB container.

Prerequisites

    Ubuntu server 16.04 -64bit
    Root privileges

Step 1 - Install Docker

In this step, we will install Docker. Docker is available in the Ubuntu repository, just update your repository and then install it.

Update ubuntu repository and upgrade:

sudo apt-get update
sudo apt-get upgrade

Install latest Docker from ubuntu repository.

sudo apt-get install -y docker.io

Start docker and enable it to start at boot time:

systemctl start docker
systemctl enable docker

The Docker services are running.

Next, you can try using docker with the command below to test it:

docker run hello-world

Hello world from docker.

Hello Docker

Step 2 - Install Docker-Compose

In the first step, we've already installed Docker. Now we will install docker-compose.

We need python-pip for the compose installation, install python and python-pip with apt:

sudo apt-get install -y python python-pip

When the installation is finished, install docker-compose with the pip command.

pip install docker-compose

Now check the docker-compose version:

docker-compose --version

Docker-compose has been installed.

Install Docker Compose on Ubuntu

Step 3 - Create and Configure the Docker Environment

In this step, we will build our docker-compose environment. We will use a non-root user, so we need to create that user now.

Add a new user named 'hakase' (choose your own user name here if you like):

useradd -m -s /bin/bash hakase
passwd hakase

Next, add the new user to the 'docker' group and restart docker.

usermod -a -G docker hakase
sudo systemctl restart docker

Now the user 'hakase' can use docker without sudo.

Next, from the root user, log into the 'hakase' user with su.

su - hakase

Create a new directory for the compose environment.

mkdir lemp-compose/
cd lemp-compose/

This is our docker-compose environment, all files that shall be in the Docker container must be in this directory. When we are using docker-compose, we need a .yml file named 'docker-compose.yml'.

In the 'lemp-compose' directory, create some new directories and a docker-compose.yml file:

touch docker-compose.yml
mkdir -p {logs,nginx,public,db-data}

    logs: Directory for Nginx log files.
    nginx: contains Nginx configuration like virtual host etc.
    public: directory for web files, index.html, and PHP info file.
    db-data: MariaDB data directory volume.

Create the log files error.log and access.log in the 'logs' directory.

touch logs/{error,access}.log

Create a new nginx virtual host configuration file in the 'nginx' directory:

vim nginx/app.conf

Paste configuration below:

upstream php {
        server phpfpm:9000;
}
 
server {
 
        server_name 193.70.36.50;
 
        error_log "/opt/bitnami/nginx/logs/myapp-error.log";
        access_log  "/opt/bitnami/nginx/logs/myapp-access.log";
 
        root /myapps;
        index index.php index.html;
 
        location / {
 
                try_files $uri $uri/ /index.php?$args;
        }
 
        location ~ \.php$ {
 
                include fastcgi.conf;
                fastcgi_intercept_errors on;
                fastcgi_pass php;
        }
 
        location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
                expires max;
                log_not_found off;
        }
}

Save the file and exit vim.

Create a new index.html file and PHP info file in the 'public' directory.

echo '<h1>LEMP Docker-Compose</h1><p><b>hakase-labs</b>' > public/index.html
echo '<?php phpinfo(); ?>' > public/info.php

Now you can see the environment directory as shown below:

tree

Docker compose project environment

Step 4 - Configuration of the docker-compose.yml file

In the previous step, we've created the directories and files needed for our containers. In this step, we want to edit the file 'docker-compose.yml'. In the docker-compose.yml file, we will define our services for the LEMP stack, the base images for each container, and the docker volumes.

Login to the 'hakase' user and edit the docker-compose.yml file with vim:

su - hakase
cd lemp-compose/
vim docker-compose.yml

- Define Nginx services

Paste the nginx configuration below:

nginx:
    image: 'bitnami/nginx'
    ports:
        - '80:80'
    links:
        - phpfpm
    volumes:
        - ./logs/access.log:/opt/bitnami/nginx/logs/myapp-access.log
        - ./logs/error.log:/opt/bitnami/nginx/logs/myapp-error.log
        - ./nginx/app.conf:/bitnami/nginx/conf/vhosts/app.conf
        - ./public:/myapps

In that configuration, we've already defined:

    nginx: services name
    image: we're using 'bitnami/nginx' docker images
    ports: expose container port 80 to the host port 80
    links: links 'nginx' service container to 'phpfpm' container
    volumes: mount local directories to the container. Mount the logs file directory, mount the Nginx virtual host configuration and mount th web root directory.

- Define PHP-fpm services

Paste the configuration below the Nginx block:

phpfpm:
    image: 'bitnami/php-fpm'
    ports:
        - '9000:9000'
    volumes:
        - ./public:/myapps

We defined here:

    phpfpm: define the service name.
    image: define base image for the phpfpm service with 'bitnami/php-fpm' image.
    ports: We are running PHP-fpm with TCP port 9000 and exposing the port 9000 to the host.
    volumes: mount the web root directory 'public' to 'myapps' on the container.

- Define the MySQL service

In the third block, paste the configuration below for the MariaDB service container:

mysql:
    image: 'mariadb'
    ports:
        - '3306:3306'
    volumes:
        - ./db-data:/var/lib/mysql
    environment:
        - MYSQL_ROOT_PASSWORD=hakase-labs123

Here you can see that we are using:

    mysql: as the service name.
    image: the container is based on 'mariadb' docker images.
    ports: service container using port 3306 for MySQL connection, and expose it to the host on port 3306 .
    volumes: db-data directory mysql
    environment: set the environment variable 'MYSQL_ROOT_PASSWORD' for the mysql root password to the docker images, executed when building the container.

- PHPMyAdmin services configuration

The last block, paste the configuration below:

phpmyadmin:
    image: 'phpmyadmin/phpmyadmin'
    restart: always
    ports:
       - '8080:80'
    links:
        - mysql:mysql
    environment:
        MYSQL_USERNAME: root
        MYSQL_ROOT_PASSWORD: hakase-labs123
        PMA_HOST: mysql

We are using a 'phpmyadmin' docker image, mapping container port 80 to 8080 on the host, link the container to the mariadb container, set restart always and set some environment variables of the docker image, including set 'PMA_HOST'.

Save the file and exit vim.

You can see full example on github.

Step 5 - Run Docker-Compose

Now we're ready to run docker-compose. Note: when you want to run docker-compose, you must be in the docker-compose project directory and make sure there is the yml file with the compose configuration.

Run the command below to spin up the LEMP stack:

docker-compose up -d

-d: running as daemon or background

You will see the result that the new containers have been created, check it with the command below:

docker-compose ps

running docker-compose

Now we've four containers running Nginx, PHP-fpm, MariaDB and PHPMyAdmin.

Step 6 - Testing

Checking ports that are used by the docker-proxy on the host.

netstat -plntu

all docker port mapped to host

We can see port 80 for the Nginx container, port 3306 for the MariaDB container, port 9000 for the php-fpm container, and port 8080 for the PHPMyAdmin container.

Access port 80 from the web browser, and you will see our index.html file.

http://serverip-address/

nginx docker container is work

Make sure PHP-fpm is running, access it from the web browser.

http://serverip-address/info.php

php-fpm docker container is working

Access the MySQL container in the MySQL shell.

docker-compose exec mysql bash
mysql -u root -p
TYPE MYSQL PASSWORD: hakase-labs123

Now create a new database:

create database hakase_db;
exit

MariaDB mysql shell container is accessible, and we've created a new database 'hakase_db'.

access mysql shell docker container

Next, access PHPMyAdmin on port 8080: http://serverip-address:8080/.

You will see the PHPMyAdmin login page, just type user name 'root' and the password is 'hakase-labs123'.

phpmyadmin docker container

You will be automatically connected to the mysql container that has been defined in the PMA_HOST environment variable.

Click 'Go' and you will see the phpmyadmin dashboard that is connected to the 'mysql' container.

phpmyadmin and mysql docker container is working

Success! The LEMP Stack is running under a docker-compose setup, consisting of four containers.
Reference

https://hub.docker.com/r/bitnami/nginx/

https://hub.docker.com/r/bitnami/php-fpm/

https://hub.docker.com/_/mariadb/

https://hub.docker.com/r/phpmyadmin/phpmyadmin/

https://docs.docker.com/compose/

https://github.com/yuzukiseo/hakase-labs/tree/master/lemp-compose

---


Dockerizing Flask Application Using Docker on Debian 10
On this page

    Prerequisites
    Getting Started
    Install Required Dependencies
    Install Docker
    Setup Flask Directory Structure
    Configure Docker to Deploy Flask
    Deploy Template Files
    Conclusion

Docker is an open-source tool designed to make it easier to create, deploy, and run applications by using containers. A container is a software package that packages up code and all its dependencies so the application runs quickly and reliably from one computing environment to another.

Flask is a popular Python web framework. It is classified as a microframework because it does not require particular tools or libraries. Compared to other frameworks it is lightweight and highly structured.

In this tutorial, we will explain how to deploy Flask application with Docker on Debian 10 server.
Prerequisites

    A server running Debian 10.
    A root password is configured on your server.

Getting Started

Before starting, it is a good idea to update your system's package to the latest version. You can update all the packages with the following command:

apt-get update -y
apt-get upgrade -y

Once all the packages are updated, restart your system to apply the changes.
Install Required Dependencies

Next, you will need to install the Nginx web server and other dependencies in your system. You can install all of them using the following command:

apt-get install nginx apt-transport-https ca-certificates curl gnupg2 software-properties-common curl -y

Once all the packages are installed, you can proceed to the next step.
Install Docker

By default, the latest version of Docker is not available in the Debian 10 repository. So it is a good idea to install it from the Docker official repository.

First, download and add the GPG key with the following command:

wget https://download.docker.com/linux/debian/gpg apt-key add gpg

Next, add the Docker repository with the following command:

add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian buster stable"

Next, update the repository and install the Docker with the following command:

apt-get update -y
apt-get install docker-ce -y

Once the installation has been completed successfully, verify the status of the Docker with the following command:

systemctl status docker

You should get the following output:

? docker.service - Docker Application Container Engine
   Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
   Active: active (running) since Sun 2020-04-19 06:26:25 UTC; 1min 8s ago
     Docs: https://docs.docker.com
 Main PID: 8883 (dockerd)
    Tasks: 10
   Memory: 46.6M
   CGroup: /system.slice/docker.service
           ??8883 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

Apr 19 06:26:24 debian10 dockerd[8883]: time="2020-04-19T06:26:24.413828757Z" level=warning msg="Your kernel does not support swap memory limit
Apr 19 06:26:24 debian10 dockerd[8883]: time="2020-04-19T06:26:24.413876690Z" level=warning msg="Your kernel does not support cgroup rt period"
Apr 19 06:26:24 debian10 dockerd[8883]: time="2020-04-19T06:26:24.413889604Z" level=warning msg="Your kernel does not support cgroup rt runtime
Apr 19 06:26:24 debian10 dockerd[8883]: time="2020-04-19T06:26:24.414115814Z" level=info msg="Loading containers: start."
Apr 19 06:26:24 debian10 dockerd[8883]: time="2020-04-19T06:26:24.788332166Z" level=info msg="Default bridge (docker0) is assigned with an IP a
Apr 19 06:26:24 debian10 dockerd[8883]: time="2020-04-19T06:26:24.972022325Z" level=info msg="Loading containers: done."
Apr 19 06:26:25 debian10 dockerd[8883]: time="2020-04-19T06:26:25.010940205Z" level=info msg="Docker daemon" commit=afacb8b7f0 graphdriver(s)=o
Apr 19 06:26:25 debian10 dockerd[8883]: time="2020-04-19T06:26:25.011145541Z" level=info msg="Daemon has completed initialization"
Apr 19 06:26:25 debian10 systemd[1]: Started Docker Application Container Engine.
Apr 19 06:26:25 debian10 dockerd[8883]: time="2020-04-19T06:26:25.074603639Z" level=info msg="API listen on /var/run/docker.sock"

At this point, Docker is installed and running. You can now proceed to the next step.
Setup Flask Directory Structure

Next, you will need to create a directory structure to hold your Flask application.

Let's create a directory named flask inside the /var/www/ directory:

mkdir -p /var/www/flask

Next, change the directory to a flask and create the directory structure for flask:

cd /var/www/flask
mkdir -p app/static
mkdir -p app/templates

The static directory is used to store images, CSS, and JavaScript files while the templates directory is used to store the HTML templates for your project.

Next, you will need to create an __init__.py file inside the app directory:

nano app/__init__.py

Add the following contents to create a Flask instance and import the logic from the views.py file:

from flask import Flask
app = Flask(__name__)
from app import views

Save and close the file then create the views.py file in your app directory.

nano app/views.py

Add the following lines

from app import app
@app.route('/')
def home():
   return "hello world!"

Save and close the file then create the uwsgi.ini file with the following command:

nano uwsgi.ini

This file will contain the uWSGI configurations for our application as shown below:

[uwsgi]
module = main
callable = app
master = true

Save and close the file when you are finished. uWSGI is a deployment option for Nginx that is both a protocol and an application server.

Next, you will need to create the main.py file to imports the Flask instance named app from the application package. You can create it with the following command:

nano main.py

Add the following line:

from app import app

Save and close the file when you are finished. Next, create a requirements.txt file for specifying the dependencies that the pip package manager will install to your Docker deployment:

nano requirements.txt

Add the following line that matches with the latest version of Flask:

Flask==1.1.2

Save and close the file when you are finished.

At this point, your Flask application has been configured successfully. You can now proceed to the next step.
Configure Docker to Deploy Flask

Next, you will need to create a Dockerfile to build and deploy a flask application.

First, create a Dockerfile with the following command:

cd /var/www/flask
nano Dockerfile

Add the following lines:

FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7
RUN apk --update add bash nano
ENV STATIC_URL /static
ENV STATIC_PATH /var/www/app/static
COPY ./requirements.txt /var/www/requirements.txt
RUN pip install -r /var/www/requirements.txt

Save and close the file when you are finished. Next, create a start.sh script to build an image from the Dockerfile and create a container from the resulting Docker image.

nano start.sh

Add the following line:

#!/bin/bash
app="docker.test"
docker build -t ${app} .
docker run -d -p 56733:80 \
  --name=${app} \
  -v $PWD:/app ${app}

Save and close the file when you are finished.

Note : Make sure the port 56733 is free and usable.

Finally, run the script using the following command:

bash start.sh

This will create the Docker image and build a container from the resulting image as shown below:

Sending build context to Docker daemon  9.728kB
Step 1/6 : FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7
python3.6-alpine3.7: Pulling from tiangolo/uwsgi-nginx-flask
48ecbb6b270e: Pull complete 
692f29ee68fa: Pull complete 
f75fc7ac1098: Pull complete 
c30e40bb471c: Pull complete 
Successfully built f5de17e1ce82
Successfully tagged docker.test:latest
22cd2bd23c9190cf2900fa1d7c55e4765a266e68c74dc1e6858872e9ebe7bdcd

You can now list the running containers with the following command:

docker ps

You should get the following output:

CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                            NAMES
22cd2bd23c91        docker.test         "/entrypoint.sh /sta…"   About a minute ago   Up About a minute   443/tcp, 0.0.0.0:56733->80/tcp   docker.test

You can also verify the Docker container by visiting the URL http://your-server-ip:56733. You should see the following screen:

Deploy Template Files

You can also deploy template files to serve static and dynamic content. You can create a home page template for your application with the following command:

nano app/templates/home.html

Add the following codes:

<!doctype html>

<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Welcome Flask</title>
  </head>

  <body>
    <h1>Home Page</h1>
    <p>This is the home page of our flask application.</p>
  </body>
</html>

Save and close the file when you are finished. Next, you will need to modify the views.py file to serve the newly created file:

nano app/views.py

Update the file as shown below:

from flask import render_template

from app import app

@app.route('/')
def home():
   return "hello world!"

@app.route('/template')
def template():
    return render_template('home.html')

Save and close the file. Then, you will need to restart the Docker containers to apply the changes.

You can restart the Docker container named docker.test with the following command:

docker stop docker.test
docker start docker.test

Now, open your web browser and type the URL http://your-server-ip:56733/template. You should see your newly created template in the following screen:

Conclusion

Congratulations! you have successfully deployed a Flask application with Docker on Debian 10 server. You can now replicate the application across different servers with minimal reconfiguration. Feel free to ask me if you have any questions.

